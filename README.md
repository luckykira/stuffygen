# stuffygen

This is my first Vue application. I wrote it in like four hours. It works as a
pure front-end stuffifier, unlike SFF Code, so all stuffifying is in the
browser, and there is feedback as you type.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
